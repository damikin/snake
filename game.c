#include "game.h"
#include "apples.h"
#include "collide.h"
#include "events.h"
#include "menu.h"
#include "player.h"
#include "render.h"
#include "settings.h"
#include "stats.h"
#include "window.h"
#include <stdio.h>
#include <SDL2/SDL.h>

/************************
 * declarations
 ************************/
#define INTERVAL 250
#define INTERVAL_MIN 50
#define INTERVAL_CHANGE 10
#define NUM_APPLES 20
#define MAX_APPLES 20
#define CELLS 25
#define WINDOW_WIDTH 500
#define WALL_COLLIDE SDL_FALSE
#define LOCKSPEED SDL_FALSE

enum {
	  MENU_START
	, MENU_QUIT
	, MENU_NEXT
	, MENU_BACK
	, MENU_SINGLEAPPLE
	, MENU_WALLCOLLIDE
	, MENU_LOCKSPEED
};

void game_quit(struct game_s *);
void game_run(struct game_s *);
void game_pause(struct game_s *);
void game_dead(struct game_s *);
void game_menu(struct game_s *);
void game_reset(struct game_s *);

void game_collide(struct game_s *);
void game_error(char *);
const char *game_gettitle(struct game_s *);
struct menu_s *game_menu_build(SDL_Renderer *);

struct game_s {
	struct window_s *win;
	struct render_s *ren;
	struct menu_s *menu;
	struct player_s *player;
	struct apple_s *apples[MAX_APPLES];

	struct stats_s stats;
	struct settings_s *set;

	int state;
	SDL_Event event;

	unsigned int last_time;
	unsigned int cur_time;
};

/************************
 * game states
 ************************/
enum {
	  GAME_QUIT
	, GAME_RUN
	, GAME_PAUSE
	, GAME_DEAD
	, GAME_MENU
	, GAME_RESET
	, GAME_NONE
};

void (*game_states[])(struct game_s *) = {
	  &game_quit
	, &game_run
	, &game_pause
	, &game_dead
	, &game_menu
	, &game_reset
};

/************************
 * exported functions
 ************************/
struct game_s *
game_new() {
	struct game_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->win = window_new("Snake", WINDOW_WIDTH, WINDOW_WIDTH);
	if(!ret->win) {
		game_error("Window Creation Failed");
		goto error_handling;
	}

	ret->ren = render_new(ret->win, CELLS);
	if(!ret->ren) {
		game_error("Render Creation Failed");
		goto error_handling;
	}

	ret->menu = game_menu_build(render_getrenderer(ret->ren));
	if(!ret->menu) {
		game_error("Menu Creation Failed");
		goto error_handling;
	}

	ret->set = set_new();
	if(!ret->set) {
		game_error("Setting Creation Failed");
		goto error_handling;
	}

	for(int i = 0; i < MAX_APPLES; i++) {
		ret->apples[i] = apple_new();
		if(!ret->apples[i]) {
			game_error("Apple Creation Failed");
			goto error_handling;
		}
	}

	ret->player = NULL;

	set_lockspeed_change(ret->set, LOCKSPEED);
	set_numapples_change(ret->set, NUM_APPLES);
	set_interval_min_change(ret->set, INTERVAL_MIN);
	set_interval_change(ret->set, INTERVAL);
	set_interval_dec_change(ret->set, INTERVAL_CHANGE);
	set_cells_change(ret->set, CELLS);
	set_wallcollide_change(ret->set, WALL_COLLIDE);

	menu_reset(ret->menu);
	ret->state = GAME_RESET;
	ret->last_time = SDL_GetTicks();

	return ret;

error_handling:
	game_delete(ret);
	return NULL;
}

void
game_delete(struct game_s *g) {
	if(!g)
		return;

	player_delete(g->player);
	menu_delete(g->menu);
	render_delete(g->ren);
	window_delete(g->win);
	set_delete(g->set);

	for(int i = 0; i < MAX_APPLES; i++)
		apple_delete(g->apples[i]);

	free(g);
}

void
game_main(struct game_s *g) {
	for(;;)
		game_states[g->state](g);
}

/************************
 * game state definitions
 ************************/
void
game_quit(struct game_s *g) {
	game_delete(g);
	exit(0);
}

void
game_run(struct game_s *g) {
	g->cur_time = SDL_GetTicks();

	switch(events_poll(&g->event)) {
	case EVENT_SELECT:
		g->state = GAME_PAUSE;
		window_settitle(g->win, game_gettitle(g));
		break;

	case EVENT_UP:
		player_dirn(g->player);
		break;

	case EVENT_RIGHT:
		player_dire(g->player);
		break;

	case EVENT_DOWN:
		player_dirs(g->player);
		break;

	case EVENT_LEFT:
		player_dirw(g->player);
		break;

	case EVENT_QUIT:
		g->state = GAME_QUIT;
		break;
	}

	if(g->cur_time - g->last_time > set_interval(g->set)) {
		player_update(g->player);
		game_collide(g);
		g->last_time = SDL_GetTicks();
	}

	render_run(g->ren, g->player, g->apples, set_numapples(g->set));
}

void
game_pause(struct game_s *g) {
	render_run(g->ren, g->player, g->apples, set_numapples(g->set));
	
	switch(events_poll_wait(&g->event)) {
	case EVENT_SELECT:
		g->state = GAME_RUN;
		window_settitle(g->win, game_gettitle(g));
		break;

	case EVENT_QUIT:
		g->state = GAME_QUIT;
		break;
	}
}

void
game_dead(struct game_s *g) {
	render_dead(g->ren);

	switch(events_poll_wait(&g->event)) {
	case EVENT_SELECT:
		g->state = GAME_RESET;
		window_settitle(g->win, game_gettitle(g));
		break;

	case EVENT_QUIT:
		g->state = GAME_QUIT;
		break;
	}
}

void
game_menu(struct game_s *g) {
	render_menu(g->ren, g->menu);

	switch(events_poll_wait(&g->event)) {
	case EVENT_DOWN:
		menu_down(g->menu);
		break;

	case EVENT_UP:
		menu_up(g->menu);
		break;

	case EVENT_SELECT:
		switch(menu_item_id(g->menu)) {
		case MENU_START:
			g->state = GAME_RUN;
			break;

		case MENU_QUIT:
			g->state = GAME_QUIT;
			break;

		case MENU_NEXT:
			menu_next(g->menu);
			break;

		case MENU_BACK:
			menu_parent(g->menu);
			break;

		case MENU_SINGLEAPPLE:
			menu_toggle(g->menu);
			if(set_numapples(g->set) == 1)
				set_numapples_change(g->set, MAX_APPLES);
			else
				set_numapples_change(g->set, 1);
			break;

		case MENU_WALLCOLLIDE:
			set_wallcollide_change(g->set, !set_wallcollide(g->set));
			menu_toggle(g->menu);
			break;

		case MENU_LOCKSPEED:
			set_lockspeed_change(g->set, !set_lockspeed(g->set));
			menu_toggle(g->menu);
			break;
		}
		break;

	case EVENT_QUIT:
		g->state = GAME_QUIT;
		break;
	}
}

void
game_reset(struct game_s *g) {
	g->state = GAME_MENU;
	g->stats.score = 0;
	g->stats.apples_eaten = 0;
	set_interval_reset(g->set);
	window_settitle(g->win, game_gettitle(g));

	apples_random(g->apples, set_numapples(g->set), set_cells(g->set));
	player_delete(g->player);
	g->player = player_new(set_cells(g->set)/2, set_cells(g->set)/2);
	if(!g->player) {
		game_error("Player failed to init!");
		g->state = GAME_QUIT;
	}
}

/************************
 * game helper functions
 * only used here
 ************************/
void
game_collide(struct game_s *g) {
try_again:
	switch(collide(g->player, g->apples,
		       set_numapples(g->set),
		       set_cells(g->set))) {

	case COLLIDE_APPLE:
		player_grow(g->player);
		g->stats.score++;
		g->stats.apples_eaten++;

		if(g->stats.apples_eaten % set_numapples(g->set) == 0)
				apples_random(g->apples,
						set_numapples(g->set),
						set_cells(g->set)
					);

		if(!set_lockspeed(g->set))
			set_interval_dec(g->set);
		break;

	case COLLIDE_WALL:
		if(!set_wallcollide(g->set)) {
			player_bound(g->player, set_cells(g->set));
			goto try_again;
		}
		/* fall through */
	case COLLIDE_TAIL:
		g->state = GAME_DEAD;
		window_settitle(g->win, game_gettitle(g));
		break;
	}
}

void
game_error(char *m) {
	SDL_ShowSimpleMessageBox(0, "Error", m, NULL);
}

const char *
game_gettitle(struct game_s *g) {
	static char buf[32];
	switch(g->state) {
	case GAME_PAUSE:
		return "Snake - Pause";

	case GAME_DEAD:
		snprintf(buf, 32 - 1, "%s - %d", "Snake", g->stats.score);
		return buf;

	default:
		return "Snake";
	}

	return NULL;
}

struct menu_s *
game_menu_build(SDL_Renderer *ren) {
	struct menu_node_s *node1, *node2;
	struct menu_item_s *item;
	SDL_Texture *texture, *select, *unselect;

	node1 = menu_new_node(NULL, 3);
	node2 = menu_new_node(node1, 4);

	/*
	 * Build the options menu
	 */
	unselect = render_helper_texture(ren, "graphics/unselect.png");
	select = render_helper_texture(ren, "graphics/select.png");

	texture = render_helper_texture(ren, "graphics/lock_speed.png");
	item = menu_new_item(texture, 2, MENU_LOCKSPEED);
	menu_add_option(item, unselect);
	menu_add_option(item, select);
	menu_add_node(node2, NULL, item);

	texture = render_helper_texture(ren, "graphics/wall_collide.png");
	item = menu_new_item(texture, 2, MENU_WALLCOLLIDE);
	menu_add_option(item, unselect);
	menu_add_option(item, select);
	menu_add_node(node2, NULL, item);

	texture = render_helper_texture(ren, "graphics/single_apple.png");
	item = menu_new_item(texture, 2, MENU_SINGLEAPPLE);
	menu_add_option(item, unselect);
	menu_add_option(item, select);
	menu_add_node(node2, NULL, item);

	texture = render_helper_texture(ren, "graphics/menu_back.png");
	item = menu_new_item(texture, 0, MENU_BACK);
	menu_add_node(node2, NULL, item);

	/*
	 * Build the main menu
	 */
	texture = render_helper_texture(ren, "graphics/menu_start.png");
	item = menu_new_item(texture, 0, MENU_START);
	menu_add_node(node1, NULL, item);

	texture = render_helper_texture(ren, "graphics/menu_options.png");
	item = menu_new_item(texture, 0, MENU_NEXT);
	menu_add_node(node1, node2, item);

	texture = render_helper_texture(ren, "graphics/menu_quit.png");
	item = menu_new_item(texture, 0, MENU_QUIT);
	menu_add_node(node1, NULL, item);

	return menu_new(node1);
}

