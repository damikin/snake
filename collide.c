#include "collide.h"
#include "tail.h"

int
collide(struct player_s *p, struct apple_s *a[], int apples, int cells) {
	if(collide_tail(p))
		return COLLIDE_TAIL;

	if(collide_wall(p, cells))
		return COLLIDE_WALL;

	if(collide_apples(p, a, apples))
		return COLLIDE_APPLE;

	return COLLIDE_NONE;
}

SDL_bool
collide_apple(struct player_s *p, struct apple_s *a) {
	struct tail_node_s *first = player_gethead(p);
	int x1, x2, y1, y2;

	x1 = tail_getx(first);
	y1 = tail_gety(first);
	x2 = apple_getx(a);
	y2 = apple_gety(a);

	if(apple_isactive(a) && x1 == x2 && y1 == y2) {
		apple_notactive(a);
		return SDL_TRUE;
	}

	return SDL_FALSE;
}

SDL_bool
collide_apples(struct player_s *p, struct apple_s *a[], int apples) {
	for(int i = 0; i < apples; i++)
		if(collide_apple(p, a[i]))
			return SDL_TRUE;

	return SDL_FALSE;
}

SDL_bool
collide_tail(struct player_s *p) {
	struct tail_node_s *first = player_gethead(p);
	struct tail_node_s *node = tail_next(first);
	int x1, x2, y1, y2;

	x1 = tail_getx(first);
	y1 = tail_gety(first);

	while(node) {
		x2 = tail_getx(node);
		y2 = tail_gety(node);

		if(x1 == x2 && y1 == y2)
			return SDL_TRUE;

		node = tail_next(node);
	}

	return SDL_FALSE;
}

SDL_bool
collide_wall(struct player_s *p, int cells) {
	struct tail_node_s *first = player_gethead(p);
	int x = tail_getx(first);
	int y = tail_gety(first);

	if(x < 0 || x >= cells
	|| y < 0 || y >= cells)
		return SDL_TRUE;

	return SDL_FALSE;
}

