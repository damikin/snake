#include "player.h"
#include <stdlib.h>

/*********************
 * declarations
 *********************/
enum {
	  DIR_NONE
	, DIR_N
	, DIR_E
	, DIR_S
	, DIR_W
};

struct player_s {
	struct tail_s *tail;
	int dir;
	int last_update_dir;
};

/*********************
 * exported functions
 *********************/
struct player_s *
player_new(int x, int y) {
	struct player_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->tail = tail_new(x, y);
	if(!ret->tail) {
		free(ret);
		return NULL;
	}

	for(int i = 0; i < 3; i++)
		player_grow(ret);

	ret->dir = DIR_E;
	ret->last_update_dir = ret->dir;

	return ret;
}

void
player_delete(struct player_s *p) {
	if(p) {
		tail_delete(p->tail);
		free(p);
	}
}

void
player_grow(struct player_s *p) {
	tail_grow(p->tail);
}

void
player_update(struct player_s *p) {
	p->last_update_dir = p->dir;
	int x = tail_getx(player_gethead(p));
	int y = tail_gety(player_gethead(p));

	switch(p->dir) {
	case DIR_N:
		y--;
		break;
	case DIR_S:
		y++;
		break;
	case DIR_E:
		x++;
		break;
	case DIR_W:
		x--;
		break;
	}

	tail_push(p->tail, x, y);
	tail_pop(p->tail);
}

struct tail_node_s *
player_gethead(struct player_s *p) {
	return tail_first(p->tail);
}

SDL_bool
player_dirn(struct player_s *p) {
	if(p->last_update_dir == DIR_S)
		return SDL_FALSE;

	p->dir = DIR_N;
	return SDL_TRUE;
}

SDL_bool
player_dirs(struct player_s *p) {
	if(p->last_update_dir == DIR_N)
		return SDL_FALSE;

	p->dir = DIR_S;
	return SDL_TRUE;
}

SDL_bool
player_dire(struct player_s *p) {
	if(p->last_update_dir == DIR_W)
		return SDL_FALSE;

	p->dir = DIR_E;
	return SDL_TRUE;
}

SDL_bool
player_dirw(struct player_s *p) {
	if(p->last_update_dir == DIR_E)
		return SDL_FALSE;

	p->dir = DIR_W;
	return SDL_TRUE;
}

void
player_bound(struct player_s *p, int bound) {
	struct tail_node_s *head = player_gethead(p);
	int x = tail_getx(head);
	int y = tail_gety(head);

	if(x < 0) {
		x += bound;
		tail_setx(head, x);
	}

	if(x >= bound) {
		x %= bound;
		tail_setx(head, x);
	}

	if(y < 0) {
		y += bound;
		tail_sety(head, y);
	}

	if(y >= bound) {
		y %= bound;
		tail_sety(head, y);
	}
}

