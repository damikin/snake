#ifndef TAIL_H_
#define TAIL_H_

#include <SDL2/SDL.h>

struct tail_s;
struct tail_node_s;

struct tail_s *tail_new(int, int);
void tail_delete(struct tail_s *);

void tail_push(struct tail_s *, int, int);
void tail_pop(struct tail_s *);
void tail_grow(struct tail_s *);

struct tail_node_s *tail_first(struct tail_s *);
struct tail_node_s *tail_next(struct tail_node_s *);
int tail_getx(struct tail_node_s *);
int tail_gety(struct tail_node_s *);
void tail_setx(struct tail_node_s *, int);
void tail_sety(struct tail_node_s *, int);

#endif

