#ifndef EVENTS_H_
#define EVENTS_H_

#include <SDL2/SDL.h>

enum {
	  EVENT_NONE
	, EVENT_QUIT
	, EVENT_SELECT

	, EVENT_UP
	, EVENT_RIGHT
	, EVENT_DOWN
	, EVENT_LEFT
};

int events_poll(SDL_Event *);
int events_poll_wait(SDL_Event *);

#endif

