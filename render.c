#include "render.h"
#include "tail.h"
#include <SDL2/SDL_image.h>

/*********************
 * declarations
 *********************/
void render_helper_setcell_normal(struct render_s *);
void render_helper_setcell_menu(struct render_s *, int);
void render_helper_setcell_dead(struct render_s *);

struct render_s {
	SDL_Renderer *ren;
	SDL_Texture *player;
	SDL_Texture *apple;
	SDL_Texture *dead;

	SDL_Rect cell;

	int num_cells;
};

/*********************
 * exported functions
 *********************/
struct render_s *
render_new(struct window_s *win, int cells) {
	struct render_s *ret = NULL;
	ret = calloc(1, sizeof(*ret));
	if(!ret)
		goto error_handling;

	ret->ren = SDL_CreateRenderer(window_getwindow(win), -1, 0);
	if(!ret->ren)
		goto error_handling;

	ret->player = render_helper_texture(ret->ren, "graphics/player.png");
	ret->apple = render_helper_texture(ret->ren, "graphics/apple.png");
	ret->dead = render_helper_texture(ret->ren, "graphics/dead.png");
	if(!ret->player || !ret->apple || !ret->dead)
		goto error_handling;

	ret->num_cells = cells;

	float scale = window_getwidth(win) / cells;
	SDL_RenderSetScale(ret->ren, scale, scale);
	SDL_SetRenderDrawColor(ret->ren, 0, 0, 0, 255);

	return ret;

error_handling:
	render_delete(ret);
	return NULL;
}

void
render_delete(struct render_s *ren) {
	if(!ren)
		return;

	if(ren->player)
		SDL_DestroyTexture(ren->player);
	if(ren->apple)
		SDL_DestroyTexture(ren->apple);
	if(ren->dead)
		SDL_DestroyTexture(ren->dead);
	if(ren->ren)
		SDL_DestroyRenderer(ren->ren);

	free(ren);
}

SDL_Renderer *
render_getrenderer(struct render_s *ren) {
	return ren->ren;
}

/* game state rendering functions
 */
void
render_run(struct render_s *ren, struct player_s *p, struct apple_s *a[],
		int num_apples) {
	SDL_RenderClear(ren->ren);

	render_helper_setcell_normal(ren);
	render_player(ren, p);
	render_apples(ren, a, num_apples);

	SDL_RenderPresent(ren->ren);
}

void
render_menu(struct render_s *ren, struct menu_s *menu) {
	SDL_RenderClear(ren->ren);

	int items = menu_item_num_items(menu);
	int choice = menu_item_choice(menu);

	render_helper_setcell_menu(ren, items);
	ren->cell.x = 2;
	ren->cell.y = 2;

	for(int i = 0; i < items; i++) {
		if(choice == i)
			SDL_RenderCopy(ren->ren, ren->player, NULL, &ren->cell);
		SDL_RenderCopy(ren->ren, menu_item_get_item(menu, i), NULL, &ren->cell);

		if(menu_item_get_option(menu, i)) {
			ren->cell.x = ren->cell.w * 2;
			SDL_RenderCopy(ren->ren, menu_item_get_option(menu, i),
					NULL, &ren->cell);
			ren->cell.x = 2;
		}

		ren->cell.y += ren->cell.h + 1;
	}

	SDL_RenderPresent(ren->ren);
}

void
render_dead(struct render_s *ren) {
	SDL_RenderClear(ren->ren);

	render_helper_setcell_dead(ren);
	ren->cell.x = 4;
	ren->cell.y = 4;

	SDL_RenderCopy(ren->ren, ren->dead, NULL, &ren->cell);

	SDL_RenderPresent(ren->ren);
}

/* individual renderings for render_run
 */
void
render_player(struct render_s *ren, struct player_s *p) {
	struct tail_node_s *node = player_gethead(p);

	while(node) {
		ren->cell.x = tail_getx(node);
		ren->cell.y = tail_gety(node);
		SDL_RenderCopy(ren->ren, ren->player, NULL, &ren->cell);

		node = tail_next(node);
	}
}

void
render_apple(struct render_s *ren, struct apple_s *a) {
	if(!apple_isactive(a))
		return;

	ren->cell.x = apple_getx(a);
	ren->cell.y = apple_gety(a);

	SDL_RenderCopy(ren->ren, ren->apple, NULL, &ren->cell);
}

void
render_apples(struct render_s *ren, struct apple_s *a[], int num_apples) {
	for(int i = 0; i < num_apples; i++)
		render_apple(ren, a[i]);
}

/*********************
 * helper functions
 * exported
 *********************/
SDL_Texture *
render_helper_texture(SDL_Renderer *ren, char *file) {
	SDL_Surface *temp = IMG_Load(file);
	SDL_Texture *ret = NULL;
	if(!temp)
		return NULL;

	ret = SDL_CreateTextureFromSurface(ren, temp);
	if(!ret)
		return NULL;

	SDL_FreeSurface(temp);
	return ret;
}

/*********************
 * helper functions
 * only used here
 *********************/
void
render_helper_setcell_normal(struct render_s *ren) {
	ren->cell.w = 1;
	ren->cell.h = 1;
}

void
render_helper_setcell_menu(struct render_s *ren, int num_items) {
	if(num_items < 5)
		num_items = 5;

	ren->cell.w = ren->num_cells / 3;
	ren->cell.h = ren->num_cells / (num_items + 1);
}

void
render_helper_setcell_dead(struct render_s *ren) {
	ren->cell.w = ren->num_cells - 8;
	ren->cell.h = ren->cell.w / 2;
}

