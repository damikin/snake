#include "events.h"

/*********************
 * declarations
 *********************/
int events_keyboard(SDL_Event *);

/*********************
 * exported functions
 *********************/
int
events_poll(SDL_Event *e) {
	while(SDL_PollEvent(e)) {
		switch(e->type) {
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			if(e->key.state == SDL_RELEASED)
				break;

			return events_keyboard(e);

		case SDL_QUIT:
			return EVENT_QUIT;
		}
	}

	return EVENT_NONE;
}

int
events_poll_wait(SDL_Event *e) {
	while(SDL_WaitEvent(e)) {
		switch(e->type) {
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			if(e->key.state == SDL_RELEASED)
				break;

			return events_keyboard(e);

		case SDL_QUIT:
			return EVENT_QUIT;
		}
	}

	return EVENT_NONE;
}

/*********************
 * helper functions
 * only used here
 *********************/
int
events_keyboard(SDL_Event *e) {
	switch(e->key.keysym.sym) {
	case SDLK_UP:
		return EVENT_UP;

	case SDLK_DOWN:
		return EVENT_DOWN;

	case SDLK_RIGHT:
		return EVENT_RIGHT;

	case SDLK_LEFT:
		return EVENT_LEFT;

	case SDLK_p:
	case SDLK_SPACE:
	case SDLK_RETURN:
		return EVENT_SELECT;

	case SDLK_q:
	case SDLK_ESCAPE:
		return EVENT_QUIT;
	}

	return EVENT_NONE;
}

