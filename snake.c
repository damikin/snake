#include "game.h"
#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>

int
main(void) {
	struct game_s *game;

	if(SDL_Init(SDL_INIT_VIDEO)) {
		fprintf(stderr, "\nUnable to init SDL: %s\n", SDL_GetError());
		return 2;
	}
	atexit(SDL_Quit);

	game = game_new();
	if(!game) return 1;
	game_main(game);

	/* HERE BE DRAGONS...
	 * ...but just in case clean up
	 */
	game_delete(game);
	return 1;
}

