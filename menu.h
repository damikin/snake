#ifndef MENU_H_
#define MENU_H_

#include <SDL2/SDL.h>

struct menu_s;
struct menu_node_s;
struct menu_item_s;

struct menu_s *menu_new(struct menu_node_s *);
struct menu_node_s *menu_new_node(struct menu_node_s *, int);
struct menu_item_s *menu_new_item(SDL_Texture *, int, int);
void menu_delete(struct menu_s *);
void menu_delete_node(struct menu_node_s *);
void menu_delete_item(struct menu_item_s *);

void menu_reset(struct menu_s *);
void menu_up(struct menu_s *);
void menu_down(struct menu_s *);
void menu_parent(struct menu_s *);
void menu_next(struct menu_s *);
void menu_toggle(struct menu_s *);

void menu_add_node(struct menu_node_s *, struct menu_node_s *, struct menu_item_s *);
void menu_add_option(struct menu_item_s *, SDL_Texture *);

int menu_item_num_items(struct menu_s *);
int menu_item_choice(struct menu_s *);
int menu_item_choice_option(struct menu_s *);
int menu_item_id(struct menu_s *);
SDL_Texture *menu_item_get_item(struct menu_s *, int);
SDL_Texture *menu_item_get_option(struct menu_s *, int);

#endif

