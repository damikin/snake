#ifndef PLAYER_H_
#define PLAYER_H_

#include "tail.h"
#include <SDL2/SDL.h>

struct player_s;

struct player_s *player_new(int, int);
void player_delete(struct player_s *);

void player_grow(struct player_s *);
void player_update(struct player_s *);

struct tail_node_s *player_gethead(struct player_s *);

SDL_bool player_dirn(struct player_s *);
SDL_bool player_dirs(struct player_s *);
SDL_bool player_dire(struct player_s *);
SDL_bool player_dirw(struct player_s *);

void player_bound(struct player_s *, int);

#endif

