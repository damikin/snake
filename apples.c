#include "apples.h"
#include <stdlib.h>
#include <time.h>

/*********************
 * declarations
 *********************/
SDL_bool apples_collide(struct apple_s *, struct apple_s *);

static SDL_bool rand_init = SDL_FALSE;

/*********************
 * definitions
 *********************/
struct apple_s {
	int x, y;
	SDL_bool active;
};

/*********************
 * exported functions
 *********************/
struct apple_s *
apple_new() {
	struct apple_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->x = 0;
	ret->y = 0;
	ret->active = SDL_FALSE;

	return ret;
}

void
apple_delete(struct apple_s *a) {
	if(a)
		free(a);
}

void
apple_random(struct apple_s *a, int cells) {
	if(!rand_init) {
		srand(time(NULL));
		rand_init = SDL_TRUE;
	}

	a->x = rand() % cells;
	a->y = rand() % cells;
	a->active = SDL_TRUE;
}

void
apples_random(struct apple_s *a[], int num, int cells) {
	for(int i = 0; i < num; i++) {
		apple_random(a[i], cells);
		for(int j = 0; j < i; j++)
			if(apples_collide(a[i], a[j])) {
				i--;
				break;
			}
	}
}

SDL_bool
apple_isactive(struct apple_s *a) {
	return a->active;
}

void
apple_notactive(struct apple_s *a) {
	a->active = SDL_FALSE;
}

int
apple_getx(struct apple_s *a) {
	return a->x;
}

int
apple_gety(struct apple_s *a) {
	return a->y;
}

/*********************
 * helper functions
 * only used here
 *********************/
SDL_bool
apples_collide(struct apple_s *a1, struct apple_s *a2) {
	if(a1->x == a2->x && a1->y == a2->y)
		return SDL_TRUE;

	return SDL_FALSE;
}

