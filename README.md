A simple snake game written in SDL2.

This Snake game is feature complete for a basic set.

Game Over screen has the score in the window title.

### Controls ###

- arrow keys to move and change direction.
- Spacebar, Enter, 'P' - selection or pausing.


### Options ###

- Lock Speed (snake doesn't go faster when eating apples)
- Single Apple (1 apple mode, or normal 20 apple mode)
- Wall Collide (wall collisions are game over, otherwise wrap around to the other side)

### Requirements ###
- SDL2
- SDL2-image
- clang

### Ubuntu quick start ###

- $ sudo apt-get install libsdl2-dev libsdl2-image-dev git
- $ git clone https://bitbucket.org/damikin/snake.git
- $ cd snake
- $ make run

