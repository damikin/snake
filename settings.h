#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <SDL2/SDL.h>

struct settings_s;

struct settings_s *set_new(void);
void set_delete(struct settings_s *);

SDL_bool set_lockspeed(struct settings_s *);
void set_lockspeed_change(struct settings_s *, SDL_bool);

int set_cells(struct settings_s *);
void set_cells_change(struct settings_s *, int);

int set_numapples(struct settings_s *);
void set_numapples_change(struct settings_s *, int);

unsigned int set_interval(struct settings_s *);
void set_interval_change(struct settings_s *, unsigned int);

void set_interval_dec(struct settings_s *);
void set_interval_dec_change(struct settings_s *, int);

unsigned int set_interval_min(struct settings_s *);
void set_interval_min_change(struct settings_s *, unsigned int);

void set_interval_reset(struct settings_s *);

SDL_bool set_wallcollide(struct settings_s *);
void set_wallcollide_change(struct settings_s *, SDL_bool);

#endif

