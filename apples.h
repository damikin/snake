#ifndef APPLES_H_
#define APPLES_H_

#include <SDL2/SDL.h>

struct apple_s;

struct apple_s *apple_new(void);
void apple_delete(struct apple_s *);

void apple_random(struct apple_s *, int);
void apples_random(struct apple_s *[], int, int);

SDL_bool apple_isactive(struct apple_s *);
void apple_notactive(struct apple_s *);
int apple_getx(struct apple_s *);
int apple_gety(struct apple_s *);

#endif

