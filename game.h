#ifndef GAME_H_
#define GAME_H_

struct game_s;

struct game_s *game_new(void);
void game_delete(struct game_s *);

void game_main(struct game_s *);

#endif

