#ifndef RENDER_H_
#define RENDER_H_

#include "apples.h"
#include "player.h"
#include "menu.h"
#include "window.h"
#include <SDL2/SDL.h>

struct render_s;

struct render_s *render_new(struct window_s *, int);
void render_delete(struct render_s *);

SDL_Renderer *render_getrenderer(struct render_s *);

void render_run(struct render_s *, struct player_s *, struct apple_s *[], int);
void render_menu(struct render_s *, struct menu_s *);
void render_dead(struct render_s *);

void render_player(struct render_s *, struct player_s *);
void render_apple(struct render_s *, struct apple_s *);
void render_apples(struct render_s *, struct apple_s *[], int);

SDL_Texture *render_helper_texture(SDL_Renderer *, char *);

#endif

