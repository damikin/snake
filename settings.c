#include "settings.h"

/*********************
 * declarations
 *********************/
struct settings_s {
	SDL_bool lock_speed;

	int cells;
	int num_apples;

	int interval;
	int interval_min;
	int interval_reset;
	int interval_dec;
	
	SDL_bool wall_collide;
};

/*********************
 * exported functions
 *********************/
struct settings_s *
set_new(void) {
	return malloc(sizeof(struct settings_s));
}

void
set_delete(struct settings_s *s) {
	free(s);
}

SDL_bool
set_lockspeed(struct settings_s *s) {
	return s->lock_speed;
}

void
set_lockspeed_change(struct settings_s *s, SDL_bool val) {
	s->lock_speed = val;
}

int
set_cells(struct settings_s *s) {
	return s->cells;
}

void
set_cells_change(struct settings_s *s, int val) {
	s->cells = val;
}

int
set_numapples(struct settings_s *s) {
	return s->num_apples;
}

void
set_numapples_change(struct settings_s *s, int val) {
	s->num_apples = val;
}

unsigned int
set_interval(struct settings_s *s) {
	return s->interval;
}

void
set_interval_change(struct settings_s *s, unsigned int val) {
	s->interval = val;
	s->interval_reset = val;
}

void
set_interval_dec(struct settings_s *s) {
	s->interval -= s->interval_dec;
	if(s->interval < s->interval_min)
		s->interval = s->interval_min;
}

void
set_interval_dec_change(struct settings_s *s, int val) {
	s->interval_dec = val;
}

unsigned int
set_interval_min(struct settings_s *s) {
	return s->interval_min;
}

void
set_interval_min_change(struct settings_s *s, unsigned int val) {
	s->interval_min = val;
}

void
set_interval_reset(struct settings_s *s) {
	s->interval = s->interval_reset;
}

SDL_bool
set_wallcollide(struct settings_s *s) {
	return s->wall_collide;
}

void
set_wallcollide_change(struct settings_s *s, SDL_bool val) {
	s->wall_collide = val;
}

