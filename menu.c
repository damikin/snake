#include "menu.h"
#include <stdlib.h>

/************************
 * definitions
 ************************/
struct menu_s {
	struct menu_node_s *top;
	struct menu_node_s *cur;
};

struct menu_node_s {
	struct menu_node_s *parent;
	struct menu_node_s **next_node;
	struct menu_item_s **items;

	int num_items;
	int choice;
};

struct menu_item_s {
	SDL_Texture *item;
	SDL_Texture **option;

	int num_options;
	int id;
	int choice;
};

/************************
 * new/delete
 ************************/
struct menu_s *
menu_new(struct menu_node_s *top) {
	struct menu_s *ret = calloc(1, sizeof(*ret));
	if(!ret)
		return NULL;

	ret->top = top;
	ret->cur = top;
	return ret;
}

struct menu_node_s *
menu_new_node(struct menu_node_s *parent, int num_items) {
	struct menu_node_s *ret = calloc(1, sizeof(*ret));
	if(!ret)
		return NULL;

	ret->next_node = calloc(num_items, sizeof(*ret->next_node));
	ret->items = calloc(num_items, sizeof(*ret->items));
	if(!ret->next_node || !ret->items)
		goto error_handling;

	ret->parent = parent;
	ret->num_items = num_items;
	return ret;

error_handling:
	menu_delete_node(ret);
	return NULL;
}

struct menu_item_s *
menu_new_item(SDL_Texture *item, int num_options, int id) {
	struct menu_item_s *ret = calloc(1, sizeof(*ret));
	if(!ret)
		return NULL;

	if(num_options > 0) {
		ret->option = calloc(num_options, sizeof(*ret->option));
		if(!ret->option) {
			menu_delete_item(ret);
			return NULL;
		}
	}

	ret->item = item;
	ret->id = id;
	ret->num_options = num_options;
	return ret;
}

void
menu_delete(struct menu_s *m) {
	if(!m)
		return;

	menu_delete_node(m->top);
	free(m);
}

void
menu_delete_node(struct menu_node_s *m) {
	if(!m)
		return;

	for(int i = 0; i < m->num_items; i++) {
		menu_delete_item(m->items[i]);
		menu_delete_node(m->next_node[i]);
	}

	free(m->items);
	free(m->next_node);
	free(m);
}

void
menu_delete_item(struct menu_item_s *m) {
	if(!m)
		return;

	if(m->item)
		SDL_DestroyTexture(m->item);

	if(m->option) {
		for(int i = 0; i < m->num_options; i++)
			if(m->option[i])
				SDL_DestroyTexture(m->option[i]);
		free(m->option);
	}

	free(m);
}

/************************
 * menu commands
 ************************/
void
menu_node_reset(struct menu_node_s *node) {
	if(!node)
		return;

	node->choice = 0;
	for(int i = 0; i < node->num_items; i++) {
		node->items[i]->choice = 0;
		menu_node_reset(node->next_node[i]);
	}
}
void
menu_reset(struct menu_s *m) {
	m->cur = m->top;
	menu_node_reset(m->cur);
}

void
menu_up(struct menu_s *m) {
	m->cur->choice--;
	if(m->cur->choice < 0)
		m->cur->choice = m->cur->num_items - 1;
}

void
menu_down(struct menu_s *m) {
	m->cur->choice++;
	if(m->cur->choice >= m->cur->num_items)
		m->cur->choice = 0;
}

void
menu_parent(struct menu_s *m) {
	if(!m->cur->parent)
		return;

	m->cur = m->cur->parent;
}

void
menu_next(struct menu_s *m) {
	int choice = m->cur->choice;
	struct menu_node_s *node = m->cur->next_node[choice];
	if(!node)
		return;

	node->choice = 0;
	m->cur = node;
}

void
menu_toggle(struct menu_s *m) {
	int choice = m->cur->choice;
	struct menu_item_s *item = m->cur->items[choice];

	item->choice++;
	if(item->choice >= item->num_options)
		item->choice = 0;
}
/************************
 * menu creation
 ************************/
void
menu_add_node(struct menu_node_s *m, struct menu_node_s *node, struct menu_item_s *item) {
	if(m->choice >= m->num_items)
		return;

	m->next_node[m->choice] = node;
	m->items[m->choice] = item;
	m->choice++;
}

void
menu_add_option(struct menu_item_s *item, SDL_Texture *option) {
	if(!item->option)
		return;

	if(item->choice >= item->num_options)
		return;

	item->option[item->choice] = option;
	item->choice++;
}

/************************
 * menu getters
 ************************/
int
menu_item_num_items(struct menu_s *m) {
	return m->cur->num_items;
}

int
menu_item_choice(struct menu_s *m) {
	return m->cur->choice;
}

int
menu_item_choice_option(struct menu_s *m) {
	return m->cur->items[m->cur->choice]->choice;
}

int
menu_item_id(struct menu_s *m) {
	return m->cur->items[m->cur->choice]->id;
}

SDL_Texture *
menu_item_get_item(struct menu_s *m, int num) {
	return m->cur->items[num]->item;
}

SDL_Texture *
menu_item_get_option(struct menu_s *m, int num) {
	struct menu_item_s *item = m->cur->items[num];

	if(item->option)
		return item->option[item->choice];

	return NULL;
}

