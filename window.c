#include "window.h"

struct window_s {
	SDL_Window *win;

	int width;
	int height;
};

struct window_s *
window_new(char *title, int w, int h) {
	struct window_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->width = w;
	ret->height = h;

	ret->win = SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		ret->width,
		ret->height,
		0
	);

	if(!ret->win) {
		free(ret);
		return NULL;
	}

	return ret;
}

void
window_delete(struct window_s *win) {
	if(win) {
		SDL_DestroyWindow(win->win);
		free(win);
	}
}

void
window_settitle(struct window_s *win, const char *title) {
	SDL_SetWindowTitle(win->win, title);
}

SDL_Window *
window_getwindow(struct window_s *win) {
	return win->win;
}

int
window_getwidth(struct window_s *win) {
	return win->width;
}

int
window_getheight(struct window_s *win) {
	return win->height;
}

