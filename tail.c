#include "tail.h"
#include <stdlib.h>

/*********************
 * declarations
 *********************/
struct tail_node_s *tail_node_new(int, int);
void tail_node_delete(struct tail_node_s *);

struct tail_s {
	struct tail_node_s *first;
	struct tail_node_s *last;
};

struct tail_node_s {
	struct tail_node_s *next;
	struct tail_node_s *prev;

	int x, y;
};

/*********************
 * exported functions
 *********************/
struct tail_s *
tail_new(int x, int y) {
	struct tail_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->first = tail_node_new(x, y);
	if(!ret->first) {
		free(ret);
		return NULL;
	}

	ret->last = ret->first;

	return ret;
}

void
tail_delete(struct tail_s *tail) {
	struct tail_node_s *node;
	while(tail->first) {
		node = tail->first;
		tail->first = tail_next(tail->first);
		tail_node_delete(node);
	}

	free(tail);
}

/* tail manipulations
 */
void
tail_push(struct tail_s *tail, int x, int y) {
	struct tail_node_s *node = tail_node_new(x, y);
	if(!node)
		return;

	node->next = tail->first;
	tail->first->prev = node;
	tail->first = node;
}

void
tail_pop(struct tail_s *tail) {
	struct tail_node_s *node = tail->last;
	tail->last = node->prev;
	tail->last->next = NULL;

	tail_node_delete(node);
}

void
tail_grow(struct tail_s *tail) {
	struct tail_node_s *node;
	node = tail_node_new(tail->last->x, tail->last->y);
	if(!node)
		return;

	tail->last->next = node;
	node->prev = tail->last;
	tail->last = node;
}

/* tail helpers
 */
struct tail_node_s *
tail_first(struct tail_s *tail) {
	return tail->first;
}

struct tail_node_s *
tail_next(struct tail_node_s *node) {
	return node->next;
}

int
tail_getx(struct tail_node_s *node) {
	return node->x;
}

int
tail_gety(struct tail_node_s *node) {
	return node->y;
}

void
tail_setx(struct tail_node_s *node, int x) {
	node->x = x;
}

void
tail_sety(struct tail_node_s *node, int y) {
	node->y = y;
}

/*********************
 * helper functions
 * only used here
 *********************/
struct tail_node_s *
tail_node_new(int x, int y) {
	struct tail_node_s *ret = malloc(sizeof(*ret));
	if(!ret)
		return NULL;

	ret->next = NULL;
	ret->prev = NULL;
	ret->x = x;
	ret->y = y;

	return ret;
}

void
tail_node_delete(struct tail_node_s *node) {
	free(node);
}

