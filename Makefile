NAME = snake
CC = clang

CF = -Wall -Werror -Wextra -pedantic -std=c11 \
 -fsanitize=undefined-trap -fsanitize-undefined-trap-on-error \
 -D_REENDTRANT -D_THREAD_SAFE \
 -I/usr/local/include

LD = -lSDL2 -lSDL2_image -pthread -L/usr/local/lib

CSRC = player.c apples.c tail.c
CSRC += window.c render.c
CSRC += collide.c events.c menu.c settings.c
CSRC += snake.c game.c

OBJS = ${CSRC:%.c=%.o}

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LD) $(CF)

%.o: %.c
	$(CC) -c $< $(CF)

run: $(NAME)
	./$(NAME)

clean:
	-rm $(NAME) a.out *.core *.o *~ .*~

depend:
	$(CC) -E -MM $(CSRC) > .depend

