#ifndef WINDOW_H_
#define WINDOW_H_

#include <SDL2/SDL.h>

struct window_s;

struct window_s *window_new(char *, int, int);
void window_delete(struct window_s *);

void window_settitle(struct window_s *, const char *);
SDL_Window *window_getwindow(struct window_s *);
int window_getwidth(struct window_s *);
int window_getheight(struct window_s *);

#endif

