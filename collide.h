#ifndef COLLISION_H_
#define COLLISION_H_

#include "apples.h"
#include "player.h"
#include <SDL2/SDL.h>

enum {
	  COLLIDE_APPLE
	, COLLIDE_TAIL
	, COLLIDE_WALL
	, COLLIDE_NONE
};

int collide(struct player_s *, struct apple_s *[], int, int);

SDL_bool collide_apple(struct player_s *, struct apple_s *);
SDL_bool collide_apples(struct player_s *, struct apple_s *[], int);

SDL_bool collide_tail(struct player_s *);
SDL_bool collide_wall(struct player_s *, int);

#endif

